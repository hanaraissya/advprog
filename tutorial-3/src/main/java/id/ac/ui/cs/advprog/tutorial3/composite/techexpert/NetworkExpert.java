package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement
    private static final double MINIMUM_SALARY = 50000.00;

    public NetworkExpert(String name, double salary) {
        if (salary < MINIMUM_SALARY) {
            throw new IllegalArgumentException("Salary must not below " + MINIMUM_SALARY);
        }
        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
