package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class SanBurg {
    public static void main(String[]args) {

        Food food  = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        System.out.println(food.getDescription() + " $" + food.cost());

        Food food2 = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        food2 = FillingDecorator.CHICKEN_MEAT.addFillingToBread(food2);
        food2 = FillingDecorator.TOMATO.addFillingToBread(food2);
        System.out.println(food2.getDescription() + " $" + food2.cost());

    }

}
