package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class FlatBreadDough implements Dough {

    @Override
    public String toString() {
        return "FlatBread style for staying bougie";
    }
}
