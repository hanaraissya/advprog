package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class WhiteClams implements Clams {

    @Override
    public String toString() {
        return "White Clams from New Haven";
    }
}
