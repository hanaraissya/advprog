package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {

    private Class<?> mozzarellaClass;

    @Before
    public void setUp() throws Exception {
        mozzarellaClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese");
    }

    @Test
    public void testMozzarellaCheeseisCheese() {
        Collection<Type> classInterface = Arrays.asList(mozzarellaClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testMozzarellaOverrideCheeseMethod() throws Exception {
        Method toString = mozzarellaClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}