package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class WhiteClamsTest {

    private Class<?> whiteClamsClass;

    @Before
    public void setUp() throws Exception {
        whiteClamsClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.WhiteClams");
    }

    @Test
    public void testWhiteClamsisClam() {
        Collection<Type> classInterface = Arrays.asList(whiteClamsClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams"))
        );
    }

    @Test
    public void testWhiteClamsOverrideClamsMethod() throws Exception {
        Method toString = whiteClamsClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}