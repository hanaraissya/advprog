package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class CheeseTest {

    private Class<?> cheseClass;

    @Before
    public void setUp() throws Exception {
        cheseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
    }

    @Test
    public  void testCheeseIsAPublicInterface() {
        int classModifiers = cheseClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testCheeseHasAbstractMethod() throws Exception {
        Method toString = cheseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

}