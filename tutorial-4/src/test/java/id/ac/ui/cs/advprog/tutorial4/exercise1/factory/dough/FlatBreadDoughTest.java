package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class FlatBreadDoughTest {

    private Class<?> flatBreadDoughClass;

    @Before
    public void setUp() throws Exception {
        flatBreadDoughClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.FlatBreadDough");
    }

    @Test
    public void testFlatBreadDoughisDough() {
        Collection<Type> classInterface = Arrays.asList(flatBreadDoughClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough"))
        );
    }

    @Test
    public void testFlatBreadDoughOverrideDoughMethod() throws Exception {
        Method toString = flatBreadDoughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}