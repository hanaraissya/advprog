package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.WhiteClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChilliSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;


public class DepokPizzaIngredientFactoryTest {

    private DepokPizzaIngredientFactory dpStore;

    @Before
    public void setUp() throws Exception {
        dpStore = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testNewYorkStoreCanCreateMatchDough() {
        assertEquals((dpStore.createDough() instanceof ThickCrustDough), true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchSauce() {
        assertEquals((dpStore.createSauce() instanceof ChilliSauce),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchCheese() {
        assertEquals((dpStore.createCheese() instanceof MozzarellaCheese),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchVeggies() {
        Veggies[] creates = dpStore.createVeggies();
        assertEquals((creates[0] instanceof BlackOlives),true);
        assertEquals((creates[1] instanceof Eggplant),true);
        assertEquals((creates[2] instanceof Pineapple),true);
        assertEquals((creates[3] instanceof Spinach),true);
    }

    @Test
    public void testNewYorkStoreCanCreateMatchClam() {
        assertEquals((dpStore.createClam() instanceof WhiteClams),true);
    }
}