package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {

    private Class<?> reggianoClass;

    @Before
    public void setUp() throws Exception {
        reggianoClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese");
    }

    @Test
    public void testReggianoCheeseisCheese() {
        Collection<Type> classInterface = Arrays.asList(reggianoClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese"))
        );
    }

    @Test
    public void testReggianoOverrideCheeseMethod() throws Exception {
        Method toString = reggianoClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}