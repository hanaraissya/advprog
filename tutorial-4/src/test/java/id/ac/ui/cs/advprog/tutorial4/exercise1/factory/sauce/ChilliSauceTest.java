package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class ChilliSauceTest {

    private Class<?> chilliSauceClass;

    @Before
    public void setUp() throws Exception {
        chilliSauceClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChilliSauce");
    }

    @Test
    public void testChilliSauceisSauce() {
        Collection<Type> classInterface = Arrays.asList(chilliSauceClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testChilliSauceOverrideSauceMethod() throws Exception {
        Method toString = chilliSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}