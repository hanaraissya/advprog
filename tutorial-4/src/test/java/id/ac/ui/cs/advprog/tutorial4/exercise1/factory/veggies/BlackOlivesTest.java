package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class BlackOlivesTest {

    private Class<?> blackOlivesClass;

    @Before
    public void setUp() throws Exception {
        blackOlivesClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives");
    }

    @Test
    public void testBlackOlivesisVeggies() {
        Collection<Type> classInterface = Arrays.asList(blackOlivesClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testBlackOlivesOverrideVeggiesMethod() throws Exception {
        Method toString = blackOlivesClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}