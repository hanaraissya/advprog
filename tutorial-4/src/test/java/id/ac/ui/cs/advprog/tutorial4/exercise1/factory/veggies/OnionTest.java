package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class OnionTest {

    private Class<?> onionClass;

    @Before
    public void setUp() throws Exception {
        onionClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion");
    }

    @Test
    public void testOnionisVeggies() {
        Collection<Type> classInterface = Arrays.asList(onionClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testOnionOverrideVeggiesMethod() throws Exception {
        Method toString = onionClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}