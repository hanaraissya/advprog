package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class PineappleTest {

    private Class<?> pineappleClass;

    @Before
    public void setUp() throws Exception {
        pineappleClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple");
    }

    @Test
    public void testPineappleisVeggies() {
        Collection<Type> classInterface = Arrays.asList(pineappleClass.getInterfaces());

        assertTrue(classInterface.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies"))
        );
    }

    @Test
    public void testPineappleOverrideVeggiesMethod() throws Exception {
        Method toString = pineappleClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}