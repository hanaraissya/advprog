package hello;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CvControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvWithVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "Arief"))
                .andExpect(content().string(containsString("Arief, I hope you&#39;re"
                        + " interested to hire me")))
                .andExpect(content().string(not("This is my CV")));
    }

    @Test
    public void cvWithoutVisitor() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not(", I hope you&#39;re interested to hire me")));
    }

    @Test
    public void cvWithNoNameVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", ""))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not(", I hope you&#39;re interested to hire me")));
    }

    @Test
    public void checkCvFullName() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Hana Raissya")))
                .andExpect(content().string(not("Anna ")));
    }

    @Test
    public void checkCvBirthDate() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("May 27 1999")))
                .andExpect(content().string(not("May 30 2001")));
    }

    @Test
    public void checkCvBirthPlace() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Padang")))
                .andExpect(content().string(not("Depok")));
    }

    @Test
    public void checkCvAddress() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Depok")))
                .andExpect(content().string(not("Jakarta Street")));
    }

    @Test
    public void checkCvUniversity() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("University of Indonesia")))
                .andExpect(content().string(not("Konoha University")));
    }

    @Test
    public void checkCvEssay() throws Exception {
        String goodEssay = "Hello, I'm Hana. I am 19 years old. "
                + "Currently pursuing my Computer Science "
                + "bachelor degree at University of Indonesia. ";
        String badEssay = "Hehe...";

        mockMvc.perform(get("/cv"))
                .andExpect(content().string(not(goodEssay)))
                .andExpect(content().string(not(badEssay)));
    }

}