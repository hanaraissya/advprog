package hello;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    Map<String, String> cvInformations = new HashMap<>();

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {

        if (visitor == null || visitor.equals("")) {
            model.addAttribute("visitorMessage", "This is my CV");
        } else {
            model.addAttribute("visitorMessage", visitor + ", I hope you're interested to hire me");
        }
        addAttrs();
        model.addAllAttributes(cvInformations);
        return "cv";

    }

    private void addAttrs() {
        cvInformations.put("myName", "Hana Raissya");
        cvInformations.put("birthDate", "May 27 1999");
        cvInformations.put("birthPlace", "Padang");
        cvInformations.put("address", "Depok");
        cvInformations.put("university", "University of Indonesia");
        cvInformations.put("essay", "Hello, I'm Hana. I am 19 years old. "
                + "Currently pursuing my Computer Science "
                + "bachelor degree at University of Indonesia. ");
    }
}
